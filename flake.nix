{
  description = "Home Manager configuration of mario";

  inputs = {
    # Specify the source of Home Manager and Nixpkgs.
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    myNeovim= {
url="git+https://gitlab.com/mzauchner/neovim/";
      inputs.nixpkgs.follows = "nixpkgs";
};
  };

  outputs = { nixpkgs, home-manager,myNeovim, ... }:
    let
      system = "x86_64-linux";
      overlayPackages = prev: final: {
        myNeovim =  myNeovim;
          pkgs = final;
};
      pkgs = import nixpkgs {
        system = "x86_64-linux";
      overlays = [ overlayPackages ];
      };
    in {
              home-manager.useUserPackages = true;
      homeConfigurations."mario" = home-manager.lib.homeManagerConfiguration {
        inherit pkgs;

        # Specify your home configuration modules here, for example,
        # the path to your home.nix.
        modules = [ ./home.nix 
];

        # Optionally use extraSpecialArgs
        # to pass through arguments to home.nix
      };
    };
}
